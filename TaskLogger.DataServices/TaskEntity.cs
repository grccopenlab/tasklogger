﻿namespace TaskLogger.DataServices
{
    public class TaskEntity : ITaskEntity
    {
        public long ID { get; set; }
        public string TaskName { get; set; }
        public bool IsActive { get; set; }
        public bool RequiresDetail { get; set; }
        public string DetailPrompt { get; set; }
    }
}
