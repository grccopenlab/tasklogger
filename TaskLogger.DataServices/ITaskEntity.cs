﻿namespace TaskLogger.DataServices
{
    public interface ITaskEntity
    {
        long ID { get; set; }
        string TaskName { get; set; }
        bool IsActive { get; set; }
        bool RequiresDetail { get; set; }
        string DetailPrompt { get; set; }
    }
}
