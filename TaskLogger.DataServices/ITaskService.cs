﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskLogger.DataServices
{
    public interface ITaskService
    {
        IEnumerable<ITaskEntity> GetTasks(bool activeOnly = true);
        bool RecordTaskForUser(string userName, long ID, string notes = "");
    }
}
