﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//
using TaskLogger.Common;

namespace TaskLogger.DataServices
{
    public class TaskService : ITaskService
    {
        DataContext database;
        DataContext Database
        {
            get
            {
                if (database == null)
                {
                    database = new DataContext(AppSettings.ConnectionString);
                }

                return database;
            }
        }

        public IEnumerable<ITaskEntity> GetTasks(bool activeOnly = true)
        {
            ITaskEntity[] tasks = { };

            string query = @"SELECT ID, TaskName, IsActive, RequiresDetail, DetailPrompt FROM Task ";

            if (activeOnly)
            {
                query += @"WHERE IsActive = True";
            }

            tasks = Database.Context.Sql(query).QueryMany((TaskEntity task, dynamic row) => 
            {
                task.ID = row.ID;
                task.TaskName = row.TaskName;
                task.IsActive = row.IsActive;
                task.RequiresDetail = row.RequiresDetail;
                task.DetailPrompt = row.DetailPrompt;

            }).ToArray();

            return tasks;
        }

        public bool RecordTaskForUser(string userName, long ID, string notes = "")
        {
            var date = DateTime.Now.ToString();

            var command = Database.Context
                .Sql(@"INSERT INTO TaskLog (UserName, DateStamp, TaskID, Notes) VALUES (@userName, @date, @taskID, @notes)")
                .Parameter("userName", userName)
                .Parameter("date", date, FluentData.DataTypes.DateTime)
                .Parameter("taskID", ID)
                .Parameter("notes", notes);

            int rowsAffected = command.Execute();

            return rowsAffected > 0;
        }
    }
}
