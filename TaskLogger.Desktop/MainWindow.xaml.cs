﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
//
using MahApps.Metro.Controls;
//
using TaskLogger.DataServices;

namespace TaskLogger.Desktop
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        internal ITaskService TaskManager { get; private set; }

        public MainWindow(ITaskService service)
        {
            InitializeComponent();

            // Avoids hard-coding the window's title.
            this.Title = GetAssemblyInfo("ProductName");
            this.TaskManager = service;
        }


        // Utility methods

        private void Exit()
        {
            Application.Current.Shutdown();
        }

        private string GetAssemblyInfo(string key)
        {
            string info = string.Empty;

            Assembly assembly = Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            switch (key)
            {
                case "CompanyName":
                    info = fvi.CompanyName;
                    break;

                case "ProductName":
                    info = fvi.ProductName;
                    break;

                case "ProductVersion":
                    info = fvi.ProductVersion;
                    break;
            }

            return info;
        }


        // Event handlers

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            Exit();
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            TasksItemsControl.ItemsSource = TaskManager.GetTasks()
                .Select(t => new CommandData 
                { 
                    ID = t.ID, 
                    Label = t.TaskName, 
                    RequiresPrompt = t.RequiresDetail, 
                    PromptLabel = t.DetailPrompt 
                });
        }

        private void OnTaskButtonClicked(object sender, RoutedEventArgs e)
        {
            var btn = (sender as FrameworkElement);
            if (btn != null)
            {
                var taskToLog = btn.DataContext as CommandData;
                if (taskToLog != null && taskToLog.ID != 0)
                {
                    bool canRecordTask = false;
                    var date = DateTime.Now.ToString();

                    //  Use System.Security.Principal.WindowsIdentity.GetCurrent().Name to get Domain\UserName.
                    //  Otherwise, Environment.UserName works fine when we just need the "Windows profile" user name.
                    string userName = Environment.UserName, notes = null;

                    if (taskToLog.RequiresPrompt)
                    {
                        var window = new InputWindow(taskToLog.PromptLabel);
                        var result = window.ShowDialog();

                        if (result != null && result == true)
                        {
                            notes = window.DetailTextBox.Text;
                            canRecordTask = !string.IsNullOrWhiteSpace(notes);
                        }
                    }
                    else
                    {
                        canRecordTask = true;
                    }

                    if (canRecordTask)
                    {
                        var ui = TaskScheduler.FromCurrentSynchronizationContext();

                        btn.IsEnabled = false;
                        StatusLabel.Content = "Logging task...";

                        Task.Run(() =>
                        {
                            TaskManager.RecordTaskForUser(userName, taskToLog.ID, notes);
                        })
                        .ContinueWith(task =>
                        {
                            if (task.Status == TaskStatus.RanToCompletion)
                            {
                                StatusLabel.Content = "Task logged!";
                            }
                            else
                            {
                                StatusLabel.Content = "Task could not be logged...";
                            }

                            btn.IsEnabled = true;
                        }, ui);
                    }
                }
            }
        }
    }
}
