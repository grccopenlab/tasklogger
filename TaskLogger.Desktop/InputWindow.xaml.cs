﻿using System.Windows;
//
using MahApps.Metro.Controls;

namespace TaskLogger.Desktop
{
    /// <summary>
    /// Interaction logic for InputWindow.xaml
    /// </summary>
    public partial class InputWindow : MetroWindow
    {
        public InputWindow(string promptLabel)
        {
            InitializeComponent();

            this.PromptTextBlock.Text = promptLabel;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(DetailTextBox.Text))
            {
                this.DialogResult = true;
            }
            else
            {
                MessageBox.Show("Please enter a valid task description.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
