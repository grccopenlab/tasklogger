﻿using System;
using System.Data.OleDb;
using System.Windows;
using System.Windows.Input;
using TaskLogger.Common;

namespace TaskLogger.Desktop
{
    public class CommandData
    {
        public long ID { get; set; }
        public string Label { get; set; }
        public string PromptLabel { get; set; }
        public bool RequiresPrompt { get; set; }

        private ICommand _applicationCommand;

        /// <summary>
        /// Gets the execute command.
        /// </summary>
        public ICommand ExecuteCommand
        {
            get
            {
                if (_applicationCommand == null)
                {
                    _applicationCommand = new ApplicationCommand(
                        param => this.Execute(),
                        param => this.CanExecute()
                    );
                }
                return _applicationCommand;
            }

        }


        /// <summary>
        /// Determines whether this instance can execute.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance can execute; otherwise, <c>false</c>.
        /// </returns>
        private bool CanExecute()
        {
            return true;
        }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        private void Execute()
        {
            try
            {
                if (this.ID != 0)
                {
                    string userName = Environment.UserName; //System.Security.Principal.WindowsIdentity.GetCurrent().Name;

                    string queryString = "INSERT INTO TaskLog (UserName, DateStamp, TaskID, Notes) VALUES (@userName, @date, @taskID, @notes)";
                    using (var connection = new OleDbConnection(AppSettings.ConnectionString))
                    {
                        var date = DateTime.Now.ToString();

                        OleDbCommand command = new OleDbCommand(queryString, connection);
                        command.Parameters.AddWithValue("@userName", userName);
                        command.Parameters.AddWithValue("@date", date);
                        command.Parameters.AddWithValue("@taskID", this.ID);
                        command.Parameters.AddWithValue("@notes", "Task logged.");

                        connection.Open();

                        command.ExecuteNonQuery();

                        connection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                ; // TODO: do something with exception
                MessageBox.Show(ex.Message);
            }
        }

    }
}
